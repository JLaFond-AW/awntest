AirWatch Network Prototype
==========================

Notes
-----

This is a sample project that outlines a few ideas for new AirWatch network base functionality. Any of the higher level `AIREASCommand` methods will extend from the `id<AWNNCommandProtocol>` objects.

Obviously, more work needs to be done.

* Incorporate a utility class/methods to tell whether the client is iOS6 or iOS7+, so that the check about whether to use `NSURLConnection` or `NSURLSession` based classes automatically, rather than taking an argument, as it does now.
* Incorporate the AirWatch authorization code rather than have it commented out, as it is currently.
* Add serialization helper class to the AirWatch New Network Command Manager and command protocol, so that each command will parse the data's XML (if applicable), etc... on the appropriate command thread, rather than doing it on the main thread.

Overview
--------

This prototype has three main parts:

* Protocols

    * *`AWNNCommandProtocol`*: the protocol that the command objects will conform to.
    * *`AWNNCommandManagerProtocol`*: the protocol that command objects will use to communicate to their manager delegate.
* Command Manager
    * *`AWNNCommandManager`*: the manager for all requests that manages the `NSOperationQueue` objects used and queues up the command requests.
    
        The command manager will route incoming requests to create either a `AWNNCommandOperation` or a `AWNNCommandTask` and queue them up in either the priority or the concurrent `NSOperationQueue`s, which will run the requests on other threads. This routing will be done using the `AWNNCommandType`.
        
        When the command finishes, the request code block will execute on the main thread with the results from the network call.
    
* Command Request Objects
    * *`AWNNCommandOperation`*: the NSOperation based class that uses `NSURLConnection` functionality for iOS6 devices.
    * *`AWNNCommandTask`*: the NSURLSessionTask wrapper that uses `NSURLSession` functionality for iOS7+ devices.

        Each command object will handle the request, handle the authorization challenge(s), conform to the `AWNNCommandProtocol`, and return the result to the called via the `RequestBlock` code block that will respond to the caller with a `NSHTTPURLResponse`, `NSData`, and `NSError`.

### Usage

There will be a single `AWNNCommandManager` object defined (perhaps on startup in the AppDelegate).

Command requests will be sent through the `AWNNCommandManager` with a `AWNNCommandType`, to route the request to the appropriate `NSOperationQueue` and in iOS7+ create the appropriate `NSURLSessionTask`.

Command requests will use the `RequestBlock` code block to allow the command to communicate back to the caller.

### Unit Testing

By having the lowest level network calls support code blocks and a strict protocol, we should be able to unit test any of the upper level commands by incorporating in mocked `NSHTTPRURLResponse`, `NSData`, and `NSError` objects to give us any type of faux response for testing.