//
//  AWNViewController.m
//  AWNTest
//
//  Created by JP LaFond on 5/2/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import "AWNViewController.h"
#import "AWNNCommandManager.h"

@interface AWNViewController ()

@property (nonatomic, readwrite, strong) AWNNCommandManager * commandManager;

@end

static NSString * const LOCATION_URL_REQUEST = @"http://api.openweathermap.org/data/2.5/find?q=Boston&mode=json";

@implementation AWNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated {
   [super viewDidAppear:animated];

   self.commandManager = [AWNNCommandManager new];
   self.commandManager.isIOS7Plus = YES; // Uses AWNNCommandTask
//   self.commandManager.shouldUseURLSession = NO; // Uses AWNNCommandOperation

   NSMutableURLRequest * urlRequest = (NSMutableURLRequest *)[self.commandManager createPOSTRequestWithURLString:LOCATION_URL_REQUEST
                                                                                                         Headers:@{@"x-files": @"the-truth-is-out-there", @"animal": @"monkey"}];
   urlRequest.cachePolicy = NSURLCacheStorageNotAllowed;

   NSLog(@"(%s) data", __PRETTY_FUNCTION__);
   NSMutableURLRequest * dataRequest = [urlRequest mutableCopy];
   [dataRequest addValue:@"data test" forHTTPHeaderField:@"dataType"];
   [self.commandManager queueRequest:dataRequest
                         CommandType:AWNNCommandTypeData
                       SecureAccount:nil
                       ProgressBlock:^(NSUInteger progressAmount) {
                          NSLog(@"(%s) data * %d *", __PRETTY_FUNCTION__, progressAmount);
                       }
                        RequestBlock:^(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError){
                           NSLog(@"data (%s) response (%@) data[%d] error (%@)", __PRETTY_FUNCTION__, response, responseData.length, responseError);
                           NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
                           NSLog(@"data (%s) responseString (%@)", __PRETTY_FUNCTION__, responseString);
                        }];

   NSLog(@"(%s) download", __PRETTY_FUNCTION__);
   NSMutableURLRequest * downloadRequest = [urlRequest mutableCopy];
   [downloadRequest addValue:@"download test" forHTTPHeaderField:@"dataType"];
   [self.commandManager queueRequest:downloadRequest
                         CommandType:AWNNCommandTypeDownload
                       SecureAccount:nil
                       ProgressBlock:^(NSUInteger progressAmount) {
                          NSLog(@"(%s) download * %d *", __PRETTY_FUNCTION__, progressAmount);
                       }
                        RequestBlock:^(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError){
                           NSLog(@"download (%s) response (%@) data[%d] error (%@)", __PRETTY_FUNCTION__, response, responseData.length, responseError);
                           NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
                           NSLog(@"download (%s) responseString (%@)", __PRETTY_FUNCTION__, responseString);
                        }];

   NSLog(@"(%s) upload", __PRETTY_FUNCTION__);
   NSMutableURLRequest * uploadRequest = [urlRequest mutableCopy];
   [uploadRequest addValue:@"upload test" forHTTPHeaderField:@"dataType"];
   [self.commandManager queueRequest:uploadRequest
                         CommandType:AWNNCommandTypeUpload
                          UploadData:[@"This is a test" dataUsingEncoding:NSASCIIStringEncoding]
                       SecureAccount:nil
                       ProgressBlock:^(NSUInteger progressAmount) {
                          NSLog(@"(%s) upload * %d *", __PRETTY_FUNCTION__, progressAmount);
                       }
                        RequestBlock:^(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError){
                           NSLog(@"upload (%s) response (%@) data[%d] error (%@)", __PRETTY_FUNCTION__, response, responseData.length, responseError);
                           NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
                           NSLog(@"upload (%s) responseString (%@)", __PRETTY_FUNCTION__, responseString);
                        }];

   NSLog(@"(%s) priority", __PRETTY_FUNCTION__);
   NSMutableURLRequest * priorityRequest = [urlRequest mutableCopy];
   [priorityRequest addValue:@"priority test" forHTTPHeaderField:@"dataType"];
   [self.commandManager queueRequest:priorityRequest
                         CommandType:AWNNCommandTypePriority
                       SecureAccount:nil
                       ProgressBlock:^(NSUInteger progressAmount) {
                          NSLog(@"(%s) priority * %d *", __PRETTY_FUNCTION__, progressAmount);
                       }
                        RequestBlock:^(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError){
                           NSLog(@"priority (%s) response (%@) data[%d] error (%@)", __PRETTY_FUNCTION__, response, responseData.length, responseError);
                           NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
                           NSLog(@"priority (%s) responseString (%@)", __PRETTY_FUNCTION__, responseString);
                        }];

   NSLog(@"(%s) Default", __PRETTY_FUNCTION__);
   NSMutableURLRequest * defaultRequest = [urlRequest mutableCopy];
   [defaultRequest addValue:@"default test" forHTTPHeaderField:@"dataType"];
   [self.commandManager queueRequest:defaultRequest
                         CommandType:AWNNCommandTypeDefault
                       SecureAccount:nil
                       ProgressBlock:^(NSUInteger progressAmount) {
                          NSLog(@"(%s) default * %d *", __PRETTY_FUNCTION__, progressAmount);
                       }
                        RequestBlock:^(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError){
                           NSLog(@"default (%s) response (%@) data[%d] error (%@)", __PRETTY_FUNCTION__, response, responseData.length, responseError);
                           NSString * responseString = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
                           NSLog(@"default (%s) responseString (%@)", __PRETTY_FUNCTION__, responseString);
                           if (responseError) {
                              self.view.backgroundColor = [UIColor redColor];
                           } else {
                              self.view.backgroundColor = [UIColor blueColor];
                           }
                        }];

//    [self.commandManager cancelAllCommands];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
