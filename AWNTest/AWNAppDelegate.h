//
//  AWNAppDelegate.h
//  AWNTest
//
//  Created by JP LaFond on 5/2/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AWNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
