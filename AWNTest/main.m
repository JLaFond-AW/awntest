//
//  main.m
//  AWNTest
//
//  Created by JP LaFond on 5/2/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AWNAppDelegate.h"

int main(int argc, char * argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([AWNAppDelegate class]));
   }
}
