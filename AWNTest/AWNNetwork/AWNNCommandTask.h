//
//  AWNNCommandTask.h
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AWNNCommandProtocol.h"

@interface AWNNCommandTask : NSObject <AWNNCommandProtocol, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate, NSURLSessionTaskDelegate>

@property (nonatomic, readonly, strong) NSURLSession * urlSession;
@property (nonatomic, readwrite, strong) NSURLSessionTask * sessionTask;
@property (nonatomic, readwrite, strong) id<AWNNCommandManagerProtocol> managerDelegate;

/**
 * @brief Let other methods know if this command task manager contains a 
 * specific \c NSURLSessionTask object.
 * @note It cannot be a specific NSURLSessionTask because there's the 
 * possibility that a NSURLSesstionDataTask could convert to a 
 * \c NSURLSessionDownloadTask, or in the case of an authorization challenge 
 * creating a new data task and this class needs to keep track of both until
 * completion.
 */
- (BOOL)containsTask:(NSURLSessionTask *)sessionTask;
/**
 * @brief init method that defines the \c urlSession and code blocks that will 
 * be needed.
 * 
 * @param request to use
 * @param urlSesssion to keep track of which session created it (and could be
 * used to create a new request in the case of an authorization change).
 * @param progressBlock to report progress through
 * @param requestBlock to report results (success or failure) through
 */
- (AWNNCommandTask *)initWithRequest:(NSURLRequest *)request
                             Session:(NSURLSession *)urlSession
                       ProgressBlock:(ProgressBlock)progressBlock
                        RequestBlock:(RequestBlock)requestBlock;

@end
