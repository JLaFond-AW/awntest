//
//  AWNNCommandManager.h
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AWNNCommandProtocol.h"
#import "AWNNDeviceUtils.h"

@interface AWNNCommandManager : NSObject

@property (nonatomic, readwrite, assign) BOOL isIOS7Plus;

/**
 * @brief cancel all pending/running commands
 */
- (void)cancelAllCommands;

/**
 * @brief Creates a POST \c NSURLRequest to be used with subsequent requests.
 *
 * @param urlString \c NSString url representation
 * @param headers \c NSDictionary of headers to add to the \c NSURLRequest
 * @return \c non-nil \c NSURLRequest object
 */
- (NSURLRequest *)createPOSTRequestWithURLString:(NSString *)urlString
                                         Headers:(NSDictionary *)headers;

/**
 * @brief Creates a POST \c NSURLRequest to be used with subsequent requests.
 *
 * @param urlString \c NSString url representation
 * @param headers \c NSDictionary of headers to add to the \c NSURLRequest
 * @param data \c NSData object to use in the multi-part mime body
 * @return \c non-nil \c NSURLRequest object
 */
- (NSURLRequest *)createPOSTRequestWithURLString:(NSString *)urlString
                                         Headers:(NSDictionary *)headers
                                            Data:(NSData *)data;

/**
 * @brief Public method to either create a \c AWNNCommandOperation or
 * \c AWNNCommandTask, as appropriate.
 *
 * @param request \c NSURLRequest request to use
 * @param commandType \c AWNNCommandType to queue request appropriately
 * @param uploadData \c NSData data to send
 * @param secureAccount \c SecureAccount to use with authentication challenges
 * @param progressBlock \c ProgressBlock code block to use to report progress
 * @param requestBlock \c RequestBlock code block to use to report results
 */
- (void)queueRequest:(NSURLRequest *)request
         CommandType:(AWNNCommandType)commandType
          UploadData:(NSData *)uploadData
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock;

/**
 * @brief Public method to either create a \c AWNNCommandOperation or
 * \c AWNNCommandTask, as appropriate.
 *
 * @param request \c NSURLRequest request to use
 * @param commandType \c AWNNCommandType to queue request appropriately
 * @param secureAccount \c SecureAccount to use with authentication challenges
 * @param progressBlock \c ProgressBlock code block to use to report progress
 * @param requestBlock \c RequestBlock code block to use to report results
 */
- (void)queueRequest:(NSURLRequest *)request
         CommandType:(AWNNCommandType)commandType
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock;

@end
