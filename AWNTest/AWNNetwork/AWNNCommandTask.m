//
//  AWNNCommandTask.m
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import "AWNNCommandTask.h"

@interface AWNNCommandTask()

@property (nonatomic, readwrite, strong) NSURLRequest * request;
@property (nonatomic, readwrite, strong) NSHTTPURLResponse * response;
@property (nonatomic, readwrite, strong) NSMutableData * data;
@property (nonatomic, readwrite, strong) NSError * error;

@property (nonatomic, readwrite, strong) SecureAccount * secureAccount;
@property (nonatomic, readwrite, strong) NSData * uploadData;

@property (nonatomic, readwrite, copy) ProgressBlock progressBlock;
@property (nonatomic, readwrite, copy) RequestBlock requestBlock;

@property (nonatomic, readwrite, assign) BOOL isExecuting;
@property (nonatomic, readwrite, assign) BOOL isFinished;
@property (nonatomic, readwrite, assign) BOOL isCancelled;

@property (nonatomic, readwrite, strong) NSMutableArray * taskList;

@property (nonatomic, readwrite, strong) NSURLSession * urlSession;

- (void)makeDone;

@end

@implementation AWNNCommandTask

@synthesize requestDate = _requestDate;

#pragma Getters/Setters

- (NSURLSessionTask *)sessionTask {
   return self.taskList.lastObject;
}

- (void)setSessionTask:(NSURLSessionTask *)sessionTask {
   if (![self.taskList containsObject:sessionTask]) {
      [self.taskList addObject:sessionTask];
   }
}

#pragma mark - Public Methods

- (BOOL)containsTask:(NSURLSessionTask *)sessionTask {
//   NSLog(@"(%s) sessionTask (%@) taskList <%@>", __PRETTY_FUNCTION__, sessionTask, self.taskList);
   return [self.taskList containsObject:sessionTask];
}

- (void) cancel {
    [self.sessionTask cancel];
    
    if (self.managerDelegate) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerDelegate cancelledCommand:self];
        });
    }
}

#pragma mark - Lifecycle Methods
- (AWNNCommandTask *)initWithRequest:(NSURLRequest *)request
                             Session:(NSURLSession *)urlSession
                       ProgressBlock:(ProgressBlock)progressBlock
                        RequestBlock:(RequestBlock)requestBlock {
   self = [super init];

   if (self) {
      self.request = request;
      self.response = nil;
      self.data = nil;
      self.error = nil;

      self.urlSession = urlSession;
      self.progressBlock = progressBlock;
      self.requestBlock = requestBlock;

      self.taskList = [NSMutableArray new];
   }

   return self;
}

#pragma mark - Private Methods

- (void)makeDone {
//   NSLog(@"(%s) requestBlock (%@) managerDelegate (%@)", __PRETTY_FUNCTION__, self.requestBlock, self.managerDelegate);
   if (self.requestBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.requestBlock(self.response, self.data, self.error);
      });
   }
   if (self.managerDelegate) {
      dispatch_async(dispatch_get_main_queue(), ^{
         [self.managerDelegate taskFinished:self Date:self.requestDate];
      });
   }
}

#pragma mark - NSURLSessionDownloadDelegate

#pragma mark Download Task Resume Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes {
//   NSLog(@"(%s) session (%@) downloadTask (%@) fileOffset (%lld) expectedTotalBytes (%lld)", __PRETTY_FUNCTION__, session, downloadTask, fileOffset, expectedTotalBytes);
}

#pragma mark Progress Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
//   NSLog(@"(%s) session (%@) downloadTask (%@) bytesWritten (%lld) totalBytesWritten (%lld) totalBytesExpectedToWrite (%lld)", __PRETTY_FUNCTION__, session, downloadTask, bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
   if (self.progressBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.progressBlock(bytesWritten);
      });
   }
}

#pragma mark Finish Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
//   NSLog(@"(%s) session (%@) downloadTask (%@) location (%@)", __PRETTY_FUNCTION__, session, downloadTask, location);
   self.data = [NSData dataWithContentsOfURL:location];
   // Alert the users of how much data was retrieved
   if (self.progressBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.progressBlock(self.data.length);
      });
   }

}

#pragma mark - NSURLSessionDataDelegate Methods
#pragma mark Response Handling

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
//   NSLog(@"(%s) session (%@) dataTask (%@) response (%@) completionHandler (%@)", __PRETTY_FUNCTION__, session, dataTask, response, completionHandler);

   self.response = (NSHTTPURLResponse *)response;
   self.data = [NSMutableData new];

   if ([self.response statusCode] == 451) {
      // http://msdn.microsoft.com/en-us/library/gg651019(v=exchg.80).aspx
      // 451 is MS Specific redirect see MS-ASHTTP (above link)

      if (self.sessionTask == dataTask) {
         [dataTask cancel];
      } else {
         NSLog(@"(%s) SessionTask (%@) != dataTask (%@)", __PRETTY_FUNCTION__, self.sessionTask, dataTask);
         [self.sessionTask cancel];
         [dataTask cancel];
      }
      NSMutableString *oldURLString = [[self.request.URL absoluteString] mutableCopy];
      [oldURLString replaceOccurrencesOfString:[self.request.URL host]
                                    withString:[self.response allHeaderFields][@"X-MS-Location"]
                                       options:NSLiteralSearch
                                         range:NSMakeRange(0, [oldURLString length])];
      NSURL *newURL = [NSURL URLWithString:oldURLString];
      NSMutableURLRequest *newRequest = [self.request mutableCopy];
      newRequest.URL = newURL;
      self.request = newRequest;

      if (self.urlSession != session) {
         NSLog(@"(%s) urlSession (%@) != session (%@)", __PRETTY_FUNCTION__, self.urlSession, session);
      }

      NSURLSessionTask * newTask = [session dataTaskWithRequest:newRequest];

      self.urlSession = session;
      self.sessionTask = newTask;
   } else if ([self.response statusCode] != 200) {
      self.error = [NSError errorWithDomain:@"AIREASCommand" code:102 userInfo:@{@"statusCode" : @([self.response statusCode])}];

      if (self.managerDelegate) {
         dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerDelegate cleanCredentialsStore:self];
         });
      }
   }
   // Handle the appropriate dataTask result...
//   completionHandler(NSURLSessionResponseCancel);
   completionHandler(NSURLSessionResponseAllow);
//   completionHandler(NSURLSessionResponseBecomeDownload);

   [self.managerDelegate cleanCredentialsStore:nil];
}

#pragma mark DownloadTask Usefulness
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
//   NSLog(@"(%s) session (%@) dataTask (%@) downloadTask (%@)", __PRETTY_FUNCTION__, session, dataTask, downloadTask);
   if ([self containsTask:dataTask]) {
      self.sessionTask = downloadTask;
   } else {
      NSLog(@"(%s) surprisingly, we don't know about this dataTask (%@)", __PRETTY_FUNCTION__, dataTask);
   }
//   NSLog(@"(%s) taskList <%@>", __PRETTY_FUNCTION__, self.taskList);
}

#pragma mark Data Handling
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
//   NSLog(@"(%s) session (%@) dataTask (%@) data[%d]/[%d]", __PRETTY_FUNCTION__, session, dataTask, data.length, self.data.length);
   [self.data appendData:data];
   if (self.progressBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.progressBlock(data.length);
      });
   }
}

#pragma mark - NSURLSessionTaskDelegate Methods
#pragma mark Finish/Error Handling
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
//   if (error) {
//      NSLog(@"(%s) session (%@) task (%@) error (%@)<%@>", __PRETTY_FUNCTION__, session, task, error.localizedDescription, error.userInfo);
//   } else {
//      NSLog(@"(%s) session (%@) task (%@)", __PRETTY_FUNCTION__, session, task);
//   }
   if (error.code == NSURLErrorUserCancelledAuthentication) {
      if (self.managerDelegate) {
         dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerDelegate authenticatorFailedWithError:error Command:self];
         });
      }
   }

   self.error = error;

   [self makeDone];
}

#pragma mark Authentication Handling
- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
//   NSLog(@"(%s) session (%@) task (%@) challenge (%@) completionHandler (%@)", __PRETTY_FUNCTION__, session, task, challenge, completionHandler);
}

#pragma mark Progress Handling
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
//   NSLog(@"(%s) session (%@) task (%@) bytesSent (%lld) totalBytesSent (%lld) totalBytesExpectedToSend (%lld)", __PRETTY_FUNCTION__, session, task, bytesSent, totalBytesSent, totalBytesExpectedToSend);
   if (self.progressBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.progressBlock(bytesSent);
      });
   }
}

@end
