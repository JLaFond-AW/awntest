//
//  AWNNUtils.m
//  EmailClient
//
//  Created by JP LaFond on 4/18/14.
//  Copyright (c) 2014 Airwatch. All rights reserved.
//

#import "AWNNDeviceUtils.h"

/**
 * Based vaguely upon the semantic versioning format
 * http://semver.org [Major].[Minor].[Patch]
 */

@interface AWNNVersionNumber()

- (AWNNVersionNumber *)initWithMajor:(NSInteger)majorNum
                               Minor:(NSInteger)minorNum
                               Patch:(NSInteger)patchNum;

@end

#pragma mark - AWNNVersionNumber internal class definition

@implementation AWNNVersionNumber

/**
 * @brief Private Lifecycle Initializer method
 * @param majorNum Major version for incompatible API changes
 * @param minorNum Minor version for backwards-compatible functionality changes
 * @param patchNum Patch version for backwards-compatible bug fixes
 * @return \c AWNNVersionNumber object
 */
- (AWNNVersionNumber *)initWithMajor:(NSInteger)majorNum
                               Minor:(NSInteger)minorNum
                               Patch:(NSInteger)patchNum {
   self = [super init];

   if (self) {
      _majorVersionNumber = majorNum;
      _minorVersionNumber = minorNum;
      _patchVersionNumber = patchNum;
   }

   return self;
}

/**
 * @brief Overwritten Lifecycle Description method for logging
 * @return \c NSString with semi-formatted <Major>.<Minor>.<Patch> version number
 */
- (NSString *)description {
   return [NSString stringWithFormat:@"%d.%d.%d", self.majorVersionNumber, self.minorVersionNumber, self.patchVersionNumber];
}

@end

@interface AWNNDeviceUtils ()

/// Helper method to convert \c BOOL to \c NSString
+ (NSString *)NSStringFromBOOL:(BOOL)boolValue;
+ (NSString *)NSStringFromUIDeviceBatteryState:(UIDeviceBatteryState)batteryState;
+ (NSString *)NSStringFromUIDeviceOrientation:(UIDeviceOrientation)deviceOrientation;
+ (NSString *)NSStringFromUIUserInterfaceIdiom:(UIUserInterfaceIdiom)userInterfaceIdiom;

@end

NSString * const DeviceBatteryLevelKey = @"BatteryLevel";
NSString * const DeviceBatteryMonitoringEnabledKey = @"BatteryMonitoringEnabled";
NSString * const DeviceBatteryStateKey = @"BatteryState";
NSString * const DeviceGenerateDeviceOrientationNotificationsKey = @"GenerateDeviceOrientationNotifications";
NSString * const DeviceIdentifierForVendorKey = @"IdentifierForVender";
NSString * const DeviceLocalizedModelKey = @"LocalizedModel";
NSString * const DeviceModelKey = @"Model";
NSString * const DeviceMultiTaskingSupportedKey = @"MultiTaskingSupported";
NSString * const DeviceNameKey = @"Name";
NSString * const DeviceOrientationKey = @"Orientation";
NSString * const DeviceProximityMonitoringEnabledKey = @"ProximityMonitoringEnabled";
NSString * const DeviceProximityStateKey = @"ProximityState";
NSString * const DeviceSystemNameKey = @"SystemName";
NSString * const DeviceSystemVersionKey = @"SystemVersion";
NSString * const DeviceUserInterfaceIdiomKey = @"UserInterfaceIdiom";

NSInteger const MajorUnitSeven = 7;

@implementation AWNNDeviceUtils

#pragma mark - Private Methods
+ (NSString *)NSStringFromBOOL:(BOOL)boolValue {
   if (boolValue) {
      return @"YES";
   }
   return @"NO";
}

+ (NSString *) NSStringFromUIDeviceBatteryState:(UIDeviceBatteryState)batteryState {
   switch (batteryState) {
      case UIDeviceBatteryStateFull:
         return @"BatteryStateFull";
      case UIDeviceBatteryStateCharging:
         return @"BatteryStateCharging";
      case UIDeviceBatteryStateUnplugged:
         return @"BatteryUnplugged";
      case UIDeviceBatteryStateUnknown:
         return @"BatteryStateUnknown";
   }
}

+ (NSString *) NSStringFromNSUUID:(NSUUID *)nsuuid {
   return nsuuid.UUIDString;
}

+ (NSString *) NSStringFromUIDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
   switch (deviceOrientation) {
      case UIDeviceOrientationFaceDown:
         return @"OrientationFaceDown";
      case UIDeviceOrientationFaceUp:
         return @"OrientationFaceUp";
      case UIDeviceOrientationLandscapeLeft:
         return @"OrientationLandscapeLeft";
      case UIDeviceOrientationLandscapeRight:
         return @"OrientationLandscapeRight";
      case UIDeviceOrientationPortrait:
         return @"OrientationPortrait";
      case UIDeviceOrientationPortraitUpsideDown:
         return @"OrientationPortraitUpsideDown";
      case UIDeviceOrientationUnknown:
         return @"OrientationUnknown";
   }
}

+ (NSString *) NSStringFromUIUserInterfaceIdiom:(UIUserInterfaceIdiom)userInterfaceIdiom {
   switch (userInterfaceIdiom) {
      case UIUserInterfaceIdiomPad:
         return @"Idiom iPad";
      case UIUserInterfaceIdiomPhone:
         return @"Idiom iPhone";
   }
}

#pragma mark - Public Methods

+ (NSDictionary *)DeviceInfo {
   UIDevice * device = [UIDevice currentDevice];
   NSDictionary * deviceInfo = @{DeviceBatteryLevelKey : [NSNumber numberWithFloat:device.batteryLevel],
                                 DeviceBatteryMonitoringEnabledKey : [self NSStringFromBOOL:device.batteryMonitoringEnabled],
                                 DeviceBatteryStateKey : [self NSStringFromUIDeviceBatteryState:device.batteryState],
                                 DeviceGenerateDeviceOrientationNotificationsKey : [self NSStringFromBOOL:device.generatesDeviceOrientationNotifications],
                                 DeviceIdentifierForVendorKey : [self NSStringFromNSUUID:device.identifierForVendor],
                                 DeviceLocalizedModelKey : device.localizedModel,
                                 DeviceModelKey : device.model,
                                 DeviceMultiTaskingSupportedKey : [self NSStringFromBOOL:device.multitaskingSupported],
                                 DeviceNameKey : device.name,
                                 DeviceOrientationKey : [self NSStringFromUIDeviceOrientation:device.orientation],
                                 DeviceProximityMonitoringEnabledKey : [self NSStringFromBOOL:device.proximityMonitoringEnabled],
                                 DeviceProximityStateKey : [self NSStringFromBOOL:device.proximityState],
                                 DeviceSystemNameKey : device.systemName,
                                 DeviceSystemVersionKey : device.systemVersion,
                                 DeviceUserInterfaceIdiomKey : [self NSStringFromUIUserInterfaceIdiom:device.userInterfaceIdiom]
                                 };
   return deviceInfo;
}

+ (AWNNVersionNumber *)VersionNumber {
   // Split the version number
   NSArray *versionComponents = [[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."];
   NSInteger majorNum, minorNum, patchNum;
   majorNum = minorNum = patchNum = 0;
   if (versionComponents.count >= 3) {
      majorNum = [versionComponents[0] intValue];
      minorNum = [versionComponents[1] intValue];
      patchNum = [versionComponents[2] intValue];
   } else if (versionComponents.count == 2) {
      majorNum = [versionComponents[0] intValue];
      minorNum = [versionComponents[1] intValue];
   } else if (versionComponents.count == 1) {
      majorNum = [versionComponents[0] intValue];
   }

   return [[AWNNVersionNumber alloc] initWithMajor:majorNum Minor:minorNum Patch:patchNum];
}

+ (BOOL)isIOS7Plus {
   AWNNVersionNumber * versionNumber = [self VersionNumber];
   return versionNumber.majorVersionNumber >= MajorUnitSeven;
}

@end
