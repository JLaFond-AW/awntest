//
//  AWNNCommandManager.m
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import "AWNNCommandManager.h"
#import "AWNNCommandOperation.h"
#import "AWNNCommandTask.h"

@interface AWNNCommandManager () <AWNNCommandManagerProtocol, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>

/// Priority queue used with either \c AWNNCommandOperation objects or with the \c prioritySession or \c backgroundSession
@property (nonatomic, readonly, strong) NSOperationQueue * priorityQueue;
/// Concurrent queue used with either \c AWNNCommandOperation objects or with the \c concurrentSession
@property (nonatomic, readonly, strong) NSOperationQueue * concurrentQueue;
/// \c NSURLSession to manage priority task requests on the \c priorityQueue
@property (nonatomic, readonly, strong) NSURLSession * prioritySession;
/// \c NSURLSession to manage concurrent task requests on the \c concurrentQueue
@property (nonatomic, readonly, strong) NSURLSession * concurrentSession;
/// \c NSURLSession to manage background task requests on the \c priorityQueue
@property (nonatomic, readonly, strong) NSURLSession * backgroundSession;
@property (nonatomic, readwrite, strong) NSData * uploadData;
/// taskList stores all outstanding \c AWNNCommandOperation or \c AWNNCommandTask objects for running/outstanding requests.
@property (nonatomic, readwrite, strong) NSMutableArray * taskList;
/// isBlockingTaskListAction is a blocking flag to ensure taskList operations safe
@property (nonatomic, readwrite, assign) __block BOOL isBlockingTaskListAction;

#warning Stored for the NSURLSessionDelegate Authentication methods that need it
/// \c SecureAccount used for authentication
/// @note Used for the \c NSURLSessionDelegate authentication method
/// \c URLSession:didReceiveChallenge:completionHandler: and
/// \c NSURLSessionTaskDelegate
/// \c URLSession:task:didReceiveChallenge:completionHandler: methods.
@property (nonatomic, readwrite, strong) SecureAccount * secureAccount;
/// lastProtectionSpace is used for authentication
@property (nonatomic, readwrite, strong) NSURLProtectionSpace * lastProtectionSpace;

/// Create a \c AWNNCommandOperation object and run it via the appropriate \c NSOperationQueue
- (void) queueOperationWithRequest:(NSURLRequest *)request
                       CommandType:(AWNNCommandType)commandType
                     SecureAccount:(SecureAccount *)secureAccount
                     ProgressBlock:(ProgressBlock)progressBlock
                      RequestBlock:(RequestBlock)requestBlock;

/// Create a \c NSURLSessionTask object and run it via the appropriate \c NSURLSession
- (void) createTaskWithRequest:(NSURLRequest *)request
                   CommandType:(AWNNCommandType)commandType
                 SecureAccount:(SecureAccount *)secureAccount
                 ProgressBlock:(ProgressBlock)progressBlock
                  RequestBlock:(RequestBlock)requestBlock;

/// Helper Method to find the \c AWNNCommandTask from the \c NSURLSessionTask
- (AWNNCommandTask *)findCommandTask:(NSURLSessionTask *)sessionTask;
/// Helper Method to find the \c AWNNCommandOperation from the a \c AWNNCommandOperation
- (AWNNCommandOperation *)findCommandOperation:(id<AWNNCommandProtocol>)command;

/// Helper Method to build a credential from the stored \c SecureAccount
- (NSURLCredential *)buildCredential;

/// Helper Method ensures that the \c taskList is updated/enumerated at the same time
- (void)waitForTaskListLockAndUnlock;

@end

@implementation AWNNCommandManager

#pragma mark - Constants
static NSUInteger const MaxConcurrentCommands = 2;

#pragma mark - Lifecycle methods

- (AWNNCommandManager *) init {
   self = [super init];

   if (self) {
      self.taskList = [NSMutableArray new];
      // Default value should be based on the device itself, but it can be
      // injected for unit testing
      self.isIOS7Plus = [AWNNDeviceUtils isIOS7Plus];
   }

   return self;
}

#pragma mark - getter methods

/**
 * @brief Defines/returns the priority queue for subsequent uses
 *
 * @return \c NSOperationQueue that handles a single operation at a time.
 */

- (NSOperationQueue *)priorityQueue {
   static dispatch_once_t onceToken;
   static NSOperationQueue * _priorityQueue;
   dispatch_once(&onceToken, ^{
      _priorityQueue = [NSOperationQueue new];
      _priorityQueue.name = @"Priority Queue";
      // There will only be a single operation running at any given time
   });
   return _priorityQueue;
}

/**
 * @brief Defines/returns the concurrent queue for subsequent uses
 * 
 * @return \c NSOperationQueue defined to support MaxConcurrentCommands (2) concurrent operations.
 */
- (NSOperationQueue *)concurrentQueue {
   static dispatch_once_t onceToken;
   static NSOperationQueue * _concurrentQueue;
   dispatch_once(&onceToken, ^{
      _concurrentQueue = [NSOperationQueue new];
      _concurrentQueue.name = @"Concurrent Queue";
      _concurrentQueue.maxConcurrentOperationCount = MaxConcurrentCommands;
   });
   return _concurrentQueue;
}

/**
 * @brief Defines/returns the URLSession for priority requests/tasks
 *
 * @return \c NSURLSession that uses the \c AWNNCommandManager::priorityQueue
 */
- (NSURLSession *)prioritySession {
   static dispatch_once_t onceToken;
   static NSURLSession * _prioritySession;

   dispatch_once(&onceToken, ^{
      NSURLSessionConfiguration * sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
      _prioritySession = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                       delegate:self
                                                  delegateQueue:self.priorityQueue];
   });

   return _prioritySession;
}

/**
 * @brief Defines/returns the URLSession for concurrent requests/tasks
 *
 * @return \c NSURLSession that uses the \c AWNNCommandManager::concurrentQueue
 */
- (NSURLSession *)concurrentSession {
   static dispatch_once_t onceToken;
   static NSURLSession * _concurrentSession;

   dispatch_once(&onceToken, ^{
      NSURLSessionConfiguration * sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
      _concurrentSession = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                         delegate:self
                                                    delegateQueue:self.concurrentQueue];
   });

   return _concurrentSession;
}

/**
 * @brief Defines/returns the URLSession for background requests/tasks
 *
 * @return \c NSURLSession that uses the \c AWNNCommandManager::priorityQueue
 */
- (NSURLSession *)backgroundSession {
   static dispatch_once_t onceToken;
   static NSURLSession * _backgroundSession;

   dispatch_once(&onceToken, ^{
      NSURLSessionConfiguration * sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"Background Session"];
      _backgroundSession = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                         delegate:self
                                                    delegateQueue:self.priorityQueue];
   });

   return _backgroundSession;
}

#pragma mark - private methods

/**
 * @brief Helper method to find command tasks from within the taskList
 * 
 * @param \c NSURLSessionTask to look for
 * @return \c AWNNCommandTask, if present; \c nil, otherwise
 */
- (AWNNCommandTask *)findCommandTask:(NSURLSessionTask *)sessionTask {
//   NSLog(@"(%s) sessionTask(%@)[%d]", __PRETTY_FUNCTION__, sessionTask, self.taskList.count);

   [self waitForTaskListLockAndUnlock];

   for (id<AWNNCommandProtocol> command in self.taskList) {
      if ([command isKindOfClass:[AWNNCommandTask class]]) {
         AWNNCommandTask * commandTask = (AWNNCommandTask *)command;
         if ([commandTask containsTask:sessionTask]) {
            self.isBlockingTaskListAction = NO;
            return commandTask;
         }
      }
   }
   NSLog(@"(%s) Unable to find sessionTask (%@)<Request (%@)> taskLists <%@>", __PRETTY_FUNCTION__, sessionTask, sessionTask.currentRequest, self.taskList);
   self.isBlockingTaskListAction = NO;
   return nil;
}

/**
 * @brief Helper method to find the command operations from within the taskList
 *
 * @param \c AWNNCommandProtocol command to look for
 * @return \c AWNNCommandOperation, if present; \c nil, otherwise
 */
- (AWNNCommandOperation *)findCommandOperation:(id<AWNNCommandProtocol>)command {
   if ([command isKindOfClass:[AWNNCommandOperation class]]) {
      if ([self.taskList containsObject:command]) {
         return (AWNNCommandOperation *)command;
      }
   }
   NSLog(@"(%s) Unable to find command (%@)<Request (%@)> taskLists <%@>", __PRETTY_FUNCTION__, command, command.request, self.taskList);
   return nil;
}

/**
 * @brief Helper method to create and queue a \c AWNNCommandOperation object 
 * in the appropriate OperationQueue, which will manage it.
 * 
 * @param request for the command
 * @param commandType \c AWNNCommandType to create
 * @param secureAccount to use
 * @param progressBlock optional
 * @param requestBlock optional
 * 
 * @note Operations (which use \c NSURLConnection) really only care about 
 * proiority and non-priority \c commandType. Everything is defined in terms 
 * of those two queues.
 */
- (void)queueOperationWithRequest:(NSURLRequest *)request
                      CommandType:(AWNNCommandType)commandType
                    SecureAccount:(SecureAccount *)secureAccount
                    ProgressBlock:(ProgressBlock)progressBlock
                     RequestBlock:(RequestBlock)requestBlock {
   AWNNCommandOperation * commandOperation = [AWNNCommandOperation new];
   commandOperation.managerDelegate = self;
   commandOperation.requestDate = [NSDate date];

   [commandOperation sendRequest:request SecureAccount:secureAccount ProgressBlock:progressBlock RequestBlock:requestBlock];

//   NSLog(@"(%s) about to add operation (%@) to queue", __PRETTY_FUNCTION__, commandOperation);
   if (commandType == AWNNCommandTypePriority) {
      commandOperation.queuePriority = NSOperationQueuePriorityNormal;
      [self.priorityQueue addOperation:commandOperation];
      if (!self.concurrentQueue.isSuspended) {
         [self.concurrentQueue setSuspended:YES];
      }
   } else {
      commandOperation.queuePriority = NSOperationQueuePriorityLow;
      [self.concurrentQueue addOperation:commandOperation];
      // If there's no running priority operations, wake up the concurrent queue
      if (self.priorityQueue.operationCount == 0) {
         if (self.concurrentQueue.isSuspended) {
            [self.concurrentQueue setSuspended:NO];
         }
      }
   }

//   NSLog(@"(%s) priorityQueue(%@)[%d]<%@> concurrentQueue(%@)[%d]<%@> operation <%@>", __PRETTY_FUNCTION__, self.priorityQueue, self.priorityQueue.operationCount, (self.priorityQueue.isSuspended ? @"Suspended" : @"NOT Suspended"), self.concurrentQueue, self.concurrentQueue.operationCount, (self.concurrentQueue.isSuspended ? @"Suspended" : @"NOT Suspended"), commandOperation);

   [self waitForTaskListLockAndUnlock];
   [self.taskList addObject:commandOperation];
   self.isBlockingTaskListAction = NO;
}

/**
 * @brief Helper method to create and begin a \c AWNNCommandTask object
 * in the appropriate \c NSURLSession, which will manage it.
 *
 * @param request for the command
 * @param commandType \c AWNNCommandType to create
 * @param secureAccount to use
 * @param progressBlock optional
 * @param requestBlock optional
 *
 * @note Tasks (which use \c NSURLSession) support different task types: 
 * \c NSURLSessionDataTask, \c NSURLSessionDownloadTask, 
 * \c NSURLSessionUploadTask managed by the \c prioritySession, 
 * \c concurrentSession, and \c backgroundSession.
 */
- (void)createTaskWithRequest:(NSURLRequest *)request
                  CommandType:(AWNNCommandType)commandType
                SecureAccount:(SecureAccount *)secureAccount
                ProgressBlock:(ProgressBlock)progressBlock
                 RequestBlock:(RequestBlock)requestBlock {
   NSURLSessionTask * sessionTask = nil;

   AWNNCommandTask * commandTask = nil;

   NSDate * requestDate = [NSDate date];

   BOOL isPriorityRequest = NO;

   switch (commandType) {
      case AWNNCommandTypeDefault:
      case AWNNCommandTypeData:
         commandTask = [[AWNNCommandTask alloc] initWithRequest:request
                                                        Session:self.concurrentSession
                                                  ProgressBlock:progressBlock
                                                   RequestBlock:requestBlock];
         sessionTask = [self.concurrentSession dataTaskWithRequest:request];
         break;
      case AWNNCommandTypePriority:
         commandTask = [[AWNNCommandTask alloc] initWithRequest:request
                                                        Session:self.prioritySession
                                                  ProgressBlock:progressBlock
                                                   RequestBlock:requestBlock];
         sessionTask = [self.prioritySession downloadTaskWithRequest:request];
         isPriorityRequest = YES;
         break;
      case AWNNCommandTypeBackground:
         commandTask = [[AWNNCommandTask alloc] initWithRequest:request
                                                        Session:self.backgroundSession
                                                  ProgressBlock:progressBlock
                                                   RequestBlock:requestBlock];
         sessionTask = [self.backgroundSession downloadTaskWithRequest:request];
         break;
      case AWNNCommandTypeDownload:
         commandTask = [[AWNNCommandTask alloc] initWithRequest:request
                                                        Session:self.concurrentSession
                                                  ProgressBlock:progressBlock
                                                   RequestBlock:requestBlock];
         sessionTask = [self.concurrentSession downloadTaskWithRequest:request];
         break;
      case AWNNCommandTypeUpload:
         commandTask = [[AWNNCommandTask alloc] initWithRequest:request
                                                        Session:self.concurrentSession
                                                  ProgressBlock:progressBlock
                                                   RequestBlock:requestBlock];
         sessionTask = [self.concurrentSession uploadTaskWithRequest:request fromData:self.uploadData];
         if (!self.uploadData) {
            NSLog(@"(%s) nil uploadData is problematic", __PRETTY_FUNCTION__);
         }
         break;
   }

   commandTask.sessionTask = sessionTask;
   commandTask.managerDelegate = self;
   commandTask.requestDate = requestDate;

   [self waitForTaskListLockAndUnlock];
   [self.taskList addObject:commandTask];
   self.isBlockingTaskListAction = NO;
   [sessionTask resume];

   if (isPriorityRequest) {
      if (!self.concurrentQueue.isSuspended) {
         [self.concurrentQueue setSuspended:YES];
      }
      [self.concurrentSession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks) {
//         NSLog(@"(%s) suspending tasks: data[%d] upload[%d] download[%d]", __PRETTY_FUNCTION__, dataTasks.count, uploadTasks.count, downloadTasks.count);
         // Suspend all non-priority concurrent tasks
         for (NSURLSessionTask * task in dataTasks) {
            [task suspend];
         }
         for (NSURLSessionTask * task in uploadTasks) {
            [task suspend];
         }
         for (NSURLSessionTask * task in downloadTasks) {
            [task suspend];
         }
      }];
   } else {
      if (self.concurrentQueue.isSuspended) {
         [self.concurrentQueue setSuspended:NO];
      }
      [self.concurrentSession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks) {
//         NSLog(@"(%s) RESUMING tasks: data[%d] upload[%d] download[%d]", __PRETTY_FUNCTION__, dataTasks.count, uploadTasks.count, downloadTasks.count);
         // Resume all non-priority concurrent tasks
         for (NSURLSessionTask * task in dataTasks) {
            [task resume];
         }
         for (NSURLSessionTask * task in uploadTasks) {
            [task resume];
         }
         for (NSURLSessionTask * task in downloadTasks) {
            [task resume];
         }
      }];

   }

//   // Get the tasks
//   [self.concurrentSession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks){
//      NSLog(@"(%s) concurrentSession tasks: data<%@>[%d] upload<%@>[%d] download<%@>[%d]", __PRETTY_FUNCTION__, dataTasks, dataTasks.count, uploadTasks, uploadTasks.count, downloadTasks, downloadTasks.count);
//   }];
//   [self.prioritySession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks){
//      NSLog(@"(%s) prioritySession tasks: data<%@>[%d] upload<%@>[%d] download<%@>[%d]", __PRETTY_FUNCTION__, dataTasks, dataTasks.count, uploadTasks, uploadTasks.count, downloadTasks, downloadTasks.count);
//   }];
}

#pragma mark Authentication Helper Methods
/**
 * @brief Helper method to build credentials for use with \c NSURLSession 
 * authorization challenge handling.
 * 
 * @return \c NSURLCredential from the the \c SecureAccount stored in this manager class.
 */
- (NSURLCredential *)buildCredential {
   /*
    //   AIRLog(@"buildCredential - username => %@, password => %@", self.secureAccount.username, self.secureAccount.password);
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.account.ntlm_domain.length > 0"];
    NSString * ntlmuser = nil;
    if ([predicate evaluateWithObject:self]) {
       ntlmuser = [NSString stringWithFormat:@"%@\\%@",self.secureAccount.ntlm_domain, self.secureAccount.username];
    } else {
       ntlmuser = self.secureAccount.username;
    }

    NSURLCredential * credentials = [NSURLCredential credentialWithUser:ntlmuser
                                                               password:self.secureAccount.password
                                                            persistence:NSURLCredentialPersistenceForSession];
    */
   NSURLCredential * credentials = nil;
   return credentials;
}

#pragma mark TaskList lock method

/**
 * @brief Helper method to allow the main thread to block around taskList 
 * manipulation. This is needed, because multiple threads can trigger deletions
 * as the taskList is being enumerated. It waits until it can acquire a lock 
 * and then locks it.
 */
- (void) waitForTaskListLockAndUnlock {
   while (self.isBlockingTaskListAction) {
      [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                               beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
   }
   self.isBlockingTaskListAction = YES;
}

#pragma mark - public methods

- (void)cancelAllCommands {
    for (id<AWNNCommandProtocol> command in self.taskList) {
        NSLog(@"(%s) cancelling request (%@)<%@>", __PRETTY_FUNCTION__, command.request, command.request.allHTTPHeaderFields[@"dataType"]);
        [command cancel];
    }
}

- (NSURLRequest *)createPOSTRequestWithURLString:(NSString *)urlString
                                         Headers:(NSDictionary *)headers {
   return [self createPOSTRequestWithURLString:urlString Headers:headers Data:nil];
}

- (NSURLRequest *)createPOSTRequestWithURLString:(NSString *)urlString
                                         Headers:(NSDictionary *)headers
                                            Data:(NSData *)data {
#warning Needs some error checking and better handing
//   NSLog(@"(%s) urlString (%@) headers <%@> data[%d]", __PRETTY_FUNCTION__, urlString, headers, data.length);
   NSMutableURLRequest * tmpURLRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
   tmpURLRequest.HTTPMethod = @"POST";
   if (headers) {
      tmpURLRequest.allHTTPHeaderFields = headers;
   }
   if (data) {
      NSLog(@"(s) currently, we don't support multi-part mime requests, as yet");
   }
   return tmpURLRequest;
}

- (void)queueRequest:(NSURLRequest *)request
         CommandType:(AWNNCommandType)commandType
          UploadData:(NSData *)uploadData
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock {
   self.uploadData = uploadData;

   [self queueRequest:request
          CommandType:commandType
        SecureAccount:secureAccount
        ProgressBlock:progressBlock
         RequestBlock:requestBlock];
}

- (void)queueRequest:(NSURLRequest *)request
         CommandType:(AWNNCommandType)commandType
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock {
//   NSLog(@"(%s) request (%@)<%@> commandType [%d] progressBlock (%@) requestBlock (%@)", __PRETTY_FUNCTION__, request, request.allHTTPHeaderFields[@"dataType"], commandType, progressBlock, requestBlock);
   // If the NSURLSessionDelegate calls, we don't know what request it's associated with, so we'll need to hold onto it here...
   self.secureAccount = secureAccount;

   if (self.isIOS7Plus) {
      [self createTaskWithRequest:request
                      CommandType:commandType
                    SecureAccount:secureAccount
                    ProgressBlock:progressBlock
                     RequestBlock:requestBlock];
   } else {
      [self queueOperationWithRequest:request
                          CommandType:commandType
                        SecureAccount:secureAccount
                        ProgressBlock:progressBlock
                         RequestBlock:requestBlock];
   }
}

#pragma mark - NSURLSessionDelegate Methods
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error {
//   NSLog(@"(%s) session (%@) error (%@)<%@>", __PRETTY_FUNCTION__, session, error.localizedDescription, error.userInfo);
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition,
                             NSURLCredential *credential))completionHandler {
//   NSLog(@"(%s) session (%@) challenge (%@) failureResponse (%@) completionHandler (%@)", __PRETTY_FUNCTION__, session, challenge, challenge.failureResponse, completionHandler);

   /*
   AIRLog(@"didReceiveAuthenticationChallenge: secureAccount (%@)", self.secureAccount);

   if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {

      BOOL ignoreSSL = [AIRSettings sharedSettings].ignoreSSLErrors;
      NSPredicate * ignoreSSLPredicate = [NSPredicate predicateWithValue:ignoreSSL];

      NSPredicate * hostPredicate = [NSPredicate predicateWithFormat:@"self.protectionSpace.host = %@",[self.secureAccount.eas_endpoint host]];

      NSPredicate * fullPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[hostPredicate, ignoreSSLPredicate]];

      if ([fullPredicate evaluateWithObject:challenge]) {
         AIRLog(@">>>>>>>>>>> Ignoring SSL Cert <<<<<<<<<<<");
         NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
         completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
      } else {
         completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
      }

   } else if ([[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodNTLM ||
              [[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodHTTPBasic) {
      self.lastProtectionSpace = [challenge protectionSpace];
      //      / *
      //       *  This is very, very important to check.  Depending on how your
      //       *  security policies are setup, you could lock your user out of his or
      //       *  her account by trying to use the wrong credentials too many times
      //       *  in a row.
      //       * /
      if ([challenge previousFailureCount] > 2) {
         completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
      } else {
         completionHandler(NSURLSessionAuthChallengeUseCredential, self.buildCredential);
      }

   } else {
      // we only support ntlm and basic
      completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
   }
    */
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session {
//   NSLog(@"(%s) session (%@)", __PRETTY_FUNCTION__, session);
}

#pragma mark - AWNNCommandManagerProtocol Methods
- (void)authenticatorFailedWithError:(NSError *)error Command:(id<AWNNCommandProtocol>)command {
//   NSLog(@"(%s) error (%@)<%@> command (%@)", __PRETTY_FUNCTION__, error.localizedDescription, error.userInfo, command);
}

- (void)cancelledCommand:(id<AWNNCommandProtocol>)command {
    NSLog(@"(%s) command (%@)[%@]", __PRETTY_FUNCTION__, command, command.request.allHTTPHeaderFields[@"dataType"]);
}

- (void)cleanCredentialsStore:(id<AWNNCommandProtocol>)command {
//   NSLog(@"(%s) command (%@) taskList <%@>", __PRETTY_FUNCTION__, command, self.taskList);

   if (self.lastProtectionSpace != nil) {
      [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:[self buildCredential] forProtectionSpace:self.lastProtectionSpace];
   }
}

- (void)taskFinished:(id<AWNNCommandProtocol>)command Date:(NSDate *)requestDate {
   NSDate * finishDate = [NSDate date];

   NSTimeInterval timeDiff = [finishDate timeIntervalSinceDate:requestDate];
   NSLog(@"(%s) time for request (%@)<%@> took %.4f", __PRETTY_FUNCTION__, command.request, command.request.allHTTPHeaderFields[@"dataType"], timeDiff);

//   if (command.error) {
//      NSLog(@"(%s) command (%@) request (%@) response (%@) data[%d] error (%@)<%@>", __PRETTY_FUNCTION__, command, command.request, command.response, command.data.length, command.error.localizedDescription, command.error.userInfo);
//   } else {
//      NSLog(@"(%s) command (%@) request (%@) response (%@) data[%d]", __PRETTY_FUNCTION__, command, command.request, command.response, command.data.length);
//   }

//   NSLog(@"(%s) priorityQueue(%@)[%d]<%@> concurrentQueue(%@)[%d]<%@> {%@}[%d]", __PRETTY_FUNCTION__, self.priorityQueue, self.priorityQueue.operationCount, (self.priorityQueue.isSuspended ? @"Suspended" : @"NOT Suspended"), self.concurrentQueue, self.concurrentQueue.operationCount, (self.concurrentQueue.isSuspended ? @"Suspended" : @"NOT Suspended"), self.taskList, self.taskList.count);

   // AWNNCommandOperation response
   if (self.priorityQueue.operationCount == 0) {
//      NSLog(@"priorityQueue <%@> concurrentQueue <%@>", (self.priorityQueue.isSuspended ? @"Suspended" : @"NOT Suspended"), (self.concurrentQueue.isSuspended ? @"Suspended" : @"NOT Suspended"));
      if (self.concurrentQueue.isSuspended) {
         [self.concurrentQueue setSuspended:NO];
      }
   }
   // AWNNCommandTask response
   if ([command isKindOfClass:[AWNNCommandTask class]]) {
      AWNNCommandTask * commandTask = (AWNNCommandTask *)command;

      // Check to see if there are any priority session tasks outstanding
      if (commandTask.urlSession == self.prioritySession) {
         [self.prioritySession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks) {
            if (dataTasks.count == 0 && uploadTasks.count == 0 && downloadTasks.count == 0) {
               // No outstanding priority requests, wake up the rest
               [self.concurrentSession getTasksWithCompletionHandler:^(NSArray * dataTasks, NSArray * uploadTasks, NSArray * downloadTasks){
//                  NSLog(@"(%s) resuming tasks: data[%d] upload[%d] download[%d]", __PRETTY_FUNCTION__, dataTasks.count, uploadTasks.count, downloadTasks.count);
                  for (NSURLSessionTask * task in dataTasks) {
                     [task resume];
                  }
                  for (NSURLSessionTask * task in uploadTasks) {
                     [task resume];
                  }
                  for (NSURLSessionTask * task in downloadTasks) {
                     [task resume];
                  }
               }];
            }
         }];
      }
   }

   [self waitForTaskListLockAndUnlock];
   if ([self.taskList containsObject:command]) {
      [self.taskList removeObject:command];
   } else {
      NSLog(@"(%s) Unknown command (%@)", __PRETTY_FUNCTION__, command);
   }
   self.isBlockingTaskListAction = NO;
}

#pragma mark - NSURLSessionDownloadDelegate

#pragma mark Download Task Resume Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
 didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes {
//   NSLog(@"(%s) session (%@) downloadTask (%@) fileOffset (%lld) expectedTotalBytes (%lld)", __PRETTY_FUNCTION__, session, downloadTask, fileOffset, expectedTotalBytes);
   AWNNCommandTask * commandTask = [self findCommandTask:downloadTask];
   [commandTask URLSession:session downloadTask:downloadTask didResumeAtOffset:fileOffset expectedTotalBytes:expectedTotalBytes];
}

#pragma mark Progress Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
//   NSLog(@"(%s) session (%@) downloadTask (%@) bytesWritten (%lld) totalBytesWritten (%lld) totalBytesExpectedToWrite (%lld)", __PRETTY_FUNCTION__, session, downloadTask, bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
   AWNNCommandTask * commandTask = [self findCommandTask:downloadTask];
   [commandTask URLSession:session downloadTask:downloadTask didWriteData:bytesWritten totalBytesWritten:totalBytesWritten totalBytesExpectedToWrite:totalBytesExpectedToWrite];
}

#pragma mark Finish Handling

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location {
//   NSLog(@"(%s) session (%@) downloadTask (%@) location (%@)", __PRETTY_FUNCTION__, session, downloadTask, location);
   AWNNCommandTask * commandTask = [self findCommandTask:downloadTask];
   [commandTask URLSession:session downloadTask:downloadTask didFinishDownloadingToURL:location];
}

#pragma mark - NSURLSessionDataDelegate Methods
#pragma mark Response Handling

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
//   NSLog(@"(%s) session (%@) dataTask (%@) response (%@)", __PRETTY_FUNCTION__, session, dataTask, response);
   AWNNCommandTask * commandTask = [self findCommandTask:dataTask];
   [commandTask URLSession:session dataTask:dataTask didReceiveResponse:response completionHandler:completionHandler];
}

#pragma mark DownloadTask Usefulness
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask {
//   NSLog(@"(%s) session (%@) dataTask (%@) downloadTask (%@)", __PRETTY_FUNCTION__, session, dataTask, downloadTask);
   AWNNCommandTask * commandTask = [self findCommandTask:dataTask];
   [commandTask URLSession:session dataTask:dataTask didBecomeDownloadTask:downloadTask];
}

#pragma mark Data Handling
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
//   NSLog(@"(%s) session (%@) dataTask (%@) data[%d]", __PRETTY_FUNCTION__, session, dataTask, data.length);
   AWNNCommandTask * commandTask = [self findCommandTask:dataTask];
   [commandTask URLSession:session dataTask:dataTask didReceiveData:data];
}

#pragma mark - NSURLSessionTaskDelegate Methods
#pragma mark Finish/Error Handling
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
//   NSLog(@"(%s) session (%@) task (%@) error (%@)<%@>", __PRETTY_FUNCTION__, session, task, error.localizedDescription, error.userInfo);
   AWNNCommandTask * commandTask = [self findCommandTask:task];
   [commandTask URLSession:session task:task didCompleteWithError:error];
}

#pragma mark Authentication Handling
- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
//   NSLog(@"(%s) session (%@) task (%@) challenge (%@) completionHandler (%@)", __PRETTY_FUNCTION__, session, task, challenge, completionHandler);
   AWNNCommandTask * commandTask = [self findCommandTask:task];
   [commandTask URLSession:session task:task didReceiveChallenge:challenge completionHandler:completionHandler];

   /*
   SecureAccount * secureAccount = commandTask.secureAccount;

   AIRLog(@"didReceiveAuthenticationChallenge: commandTask (%@) secureAccount (%@)", commandTask, secureAccount);

   if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {

      BOOL ignoreSSL = [AIRSettings sharedSettings].ignoreSSLErrors;
      NSPredicate * ignoreSSLPredicate = [NSPredicate predicateWithValue:ignoreSSL];

      NSPredicate * hostPredicate = [NSPredicate predicateWithFormat:@"self.protectionSpace.host = %@",[secureAccount.eas_endpoint host]];

      NSPredicate * fullPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[hostPredicate, ignoreSSLPredicate]];

      if ([fullPredicate evaluateWithObject:challenge]) {
         AIRLog(@">>>>>>>>>>> Ignoring SSL Cert <<<<<<<<<<<");
         NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
         completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
      } else {
         completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
      }

   } else if ([[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodNTLM ||
              [[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodHTTPBasic) {
      self.lastProtectionSpace = [challenge protectionSpace];
      //      / *
      //       *  This is very, very important to check.  Depending on how your
      //       *  security policies are setup, you could lock your user out of his or
      //       *  her account by trying to use the wrong credentials too many times
      //       *  in a row.
      //       * /
      if ([challenge previousFailureCount] > 2) {
         completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
      } else {
         completionHandler(NSURLSessionAuthChallengeUseCredential, self.buildCredential);
      }

   } else {
      // we only support ntlm and basic
      completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
   }
    */
}

#pragma mark Progress Handling
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend {
//   NSLog(@"(%s) session (%@) task (%@) bytesSent (%lld) totalBytesSent (%lld) totalBytesExpectedToSend (%lld)", __PRETTY_FUNCTION__, session, task, bytesSent, totalBytesSent, totalBytesExpectedToSend);
   AWNNCommandTask * commandTask = [self findCommandTask:task];
   [commandTask URLSession:session task:task didSendBodyData:bytesSent totalBytesSent:totalBytesSent totalBytesExpectedToSend:totalBytesExpectedToSend];
}

@end
