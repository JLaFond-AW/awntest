//
//  AWNNUtils.h
//  EmailClient
//
//  Created by JP LaFond on 5/16/14.
//  Copyright (c) 2014 Airwatch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AWNNVersionNumber : NSObject

@property (nonatomic, readonly, assign) NSInteger majorVersionNumber;
@property (nonatomic, readonly, assign) NSInteger minorVersionNumber;
@property (nonatomic, readonly, assign) NSInteger patchVersionNumber;

@end

@interface AWNNDeviceUtils : NSObject

FOUNDATION_EXPORT NSString * const DeviceBatteryLevelKey;
FOUNDATION_EXPORT NSString * const DeviceBatteryMonitoringEnabledKey;
FOUNDATION_EXPORT NSString * const DeviceBatteryStateKey;
FOUNDATION_EXPORT NSString * const DeviceGenerateDeviceOrientationNotificationsKey;
FOUNDATION_EXPORT NSString * const DeviceIdentifierForVendorKey;
FOUNDATION_EXPORT NSString * const DeviceLocalizedModelKey;
FOUNDATION_EXPORT NSString * const DeviceModelKey;
FOUNDATION_EXPORT NSString * const DeviceMultiTaskingSupportedKey;
FOUNDATION_EXPORT NSString * const DeviceNameKey;
FOUNDATION_EXPORT NSString * const DeviceOrientationKey;
FOUNDATION_EXPORT NSString * const DeviceProximityMonitoringEnabledKey;
FOUNDATION_EXPORT NSString * const DeviceProximityStateKey;
FOUNDATION_EXPORT NSString * const DeviceSystemNameKey;
FOUNDATION_EXPORT NSString * const DeviceSystemVersionKey;
FOUNDATION_EXPORT NSString * const DeviceUserInterfaceIdiomKey;

+ (NSDictionary *)DeviceInfo;
+ (AWNNVersionNumber *)VersionNumber;
+ (BOOL)isIOS7Plus;

@end
