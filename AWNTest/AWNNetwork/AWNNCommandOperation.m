//
//  AWNNCommandOperation.m
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import "AWNNCommandOperation.h"

@interface AWNNCommandOperation() <NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, readwrite, strong) NSURLRequest * request;
@property (nonatomic, readwrite, strong) NSHTTPURLResponse * response;
@property (nonatomic, readwrite, strong) NSMutableData * data;
@property (nonatomic, readwrite, strong) NSError * error;

@property (nonatomic, readwrite, strong) SecureAccount * secureAccount;
@property (nonatomic, readwrite, strong) NSData * uploadData;

@property (nonatomic, readwrite, copy) ProgressBlock progressBlock;
@property (nonatomic, readwrite, copy) RequestBlock requestBlock;

@property (nonatomic, readwrite, assign) BOOL isExecuting;
@property (nonatomic, readwrite, assign) BOOL isFinished;
@property (nonatomic, readwrite, assign) BOOL isCancelled;

@property (nonatomic, readwrite, strong) NSURLConnection * urlConnection;
@property (nonatomic, readwrite, strong) NSURLProtectionSpace *lastProtectionSpace;

@property (nonatomic, readwrite, strong) NSMutableSet * allowedAuths;
@property (nonatomic, readwrite, strong) NSMutableSet * badAuths;

- (NSPredicate *)allowedAuthenticationMethodsPredicate;
- (NSURLCredential *) buildCredential;
- (void)cleanCredentialStore;
- (void)authenticationDidFailWithError:(NSError *)error;

- (void)makeRun;

@end

@implementation AWNNCommandOperation

@synthesize requestDate = _requestDate;

static NSCache *predicateCache;

#pragma mark - Lifecycle Methods

- (AWNNCommandOperation *)init {
   self = [super init];

   if (self) {
      self.request = nil;
      self.response = nil;
      self.data = nil;
      self.error = nil;

      self.allowedAuths = [NSMutableSet new];
      self.badAuths = [NSMutableSet new];
   }

   return self;
}

#pragma mark - Private Methods

/**
 * @brief Helper method to make the class ready to run
 */
- (void)makeRun {
//   NSLog(@"(%s)", __PRETTY_FUNCTION__);
   [self willChangeValueForKey:@"isExecuting"];
   [self willChangeValueForKey:@"isFinished"];

   self.isExecuting = YES;
   self.isFinished = NO;

   [self didChangeValueForKey:@"isFinished"];
   [self didChangeValueForKey:@"isExecuting"];
}

#pragma mark Authentication Methods
- (NSPredicate *) allowedAuthenticationMethodsPredicate {
   if (predicateCache == nil) {
      predicateCache = [[NSCache alloc] init];
   }

   NSPredicate * predicate = [predicateCache objectForKey:@"allowedAuthenticationMethodsPredicate"];

   if (predicate == nil) {
      NSArray *basePreds = @[ [NSPredicate predicateWithFormat:@"self.authenticationMethod == %@",NSURLAuthenticationMethodServerTrust],
                              [NSPredicate predicateWithFormat:@"self.authenticationMethod == %@",NSURLAuthenticationMethodHTTPBasic]];

      predicate = [NSCompoundPredicate orPredicateWithSubpredicates:basePreds];

      [predicateCache setObject:predicate forKey:@"allowedAuthenticationMethodsPredicate"];
   }

   return predicate;
}

- (void)authenticationDidFailWithError:(NSError *)error {
//   /*
    NSAssert([NSThread isMainThread], @"Not running on main thread!");
   /*
    AIRAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate authenticationDidFailWithError:error];
    */
}

- (NSURLCredential *) buildCredential {
   /*
    //   AIRLog(@"buildCredential - username => %@, password => %@", self.secureAccount.username, self.secureAccount.password);
   NSPredicate *_pred = [NSPredicate predicateWithFormat:@"self.account.ntlm_domain.length > 0"];
   NSString *ntlmuser = nil;
   if ([_pred evaluateWithObject:self]) {
      ntlmuser = [NSString stringWithFormat:@"%@\\%@",self.secureAccount.ntlm_domain, self.secureAccount.username];
   } else {
      ntlmuser = self.secureAccount.username;
   }

   NSURLCredential * credentials = [NSURLCredential credentialWithUser:ntlmuser password:self.secureAccount.password persistence:NSURLCredentialPersistenceForSession];
    */
   NSURLCredential * credentials = nil;
   return credentials;
}

- (void) cleanCredentialStore {
   if (self.lastProtectionSpace != nil) {
      [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:[self buildCredential]
                                                      forProtectionSpace:self.lastProtectionSpace];
   }
}

#pragma mark - Public Methods

#pragma mark - AWNNCommandProtocol Methods

- (void)sendRequest:(NSURLRequest *)urlRequest
      SecureAccount:(SecureAccount *)secureAccount
      ProgressBlock:(ProgressBlock)progressBlock
       RequestBlock:(RequestBlock)requestBlock {
   [self sendRequest:urlRequest
                Data:nil
       SecureAccount:secureAccount
       ProgressBlock:progressBlock
        RequestBlock:requestBlock];
}

- (void)sendRequest:(NSURLRequest *)urlRequest
               Data:(NSData *)data
      SecureAccount:(SecureAccount *)secureAccount
      ProgressBlock:(ProgressBlock)progressBlock
       RequestBlock:(RequestBlock)requestBlock {
   self.request = urlRequest;
   self.data = [NSMutableData new];
#warning Multi-part request for data transfer?
   self.uploadData = data;
   self.secureAccount = secureAccount;
   self.progressBlock = progressBlock;
   self.requestBlock = requestBlock;
}

#pragma mark - NSOperation Methods
- (void)main {
   // The runLoop is needed to allow this to run asynchronously
   NSRunLoop * runLoop = [NSRunLoop currentRunLoop];
   self.urlConnection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:NO];
   [self.urlConnection scheduleInRunLoop:runLoop forMode:NSRunLoopCommonModes];
   [self.urlConnection start];
   [runLoop run];
}

- (void) start {
   [self makeRun];
   [self main];
}

- (void) cancel {
   [self.urlConnection cancel];
   self.urlConnection = nil;
   self.data = nil;
   self.error = [NSError errorWithDomain:@"cancel" code:500 userInfo:@{NSLocalizedDescriptionKey : @"cancelled operation"}];

   [self willChangeValueForKey:@"isCancelled"];
   self.isCancelled = YES;
   [self didChangeValueForKey:@"isCancelled"];
    
    if (self.managerDelegate) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.managerDelegate cancelledCommand:self];
        });
    }

   [self makeDone];
}

- (void)makeDone {
//   NSLog(@"(%s) isExecuting (%@) isFinished (%@)", __PRETTY_FUNCTION__, (self.isExecuting ? @"Executing" : @"NOT Executing"), (self.isFinished ? @"Finished" : @"NOT Finished"));
   [self willChangeValueForKey:@"isExecuting"];
   [self willChangeValueForKey:@"isFinished"];

   self.isExecuting = NO;
   self.isFinished = YES;

   [self didChangeValueForKey:@"isFinished"];
   [self didChangeValueForKey:@"isExecuting"];

   if (self.requestBlock) {
      dispatch_async(dispatch_get_main_queue(), ^{
         self.requestBlock(self.response, self.data, self.error);
      });
   }

   if (self.managerDelegate) {
      dispatch_async(dispatch_get_main_queue(), ^{
         [self.managerDelegate taskFinished:self Date:self.requestDate];
      });
   }
//   NSLog(@"(%s) isExecuting (%@) isFinished (%@) requestBlock (%@)", __PRETTY_FUNCTION__, (self.isExecuting ? @"Executing" : @"NOT Executing"), (self.isFinished ? @"Finished" : @"NOT Finished"), self.requestBlock);
}

#pragma mark Getters
- (BOOL)isConcurrent {
   return YES;
}

- (BOOL)isFinished {
   return _isFinished;
}

- (BOOL)isCancelled {
   return _isCancelled;
}

- (BOOL)isExecuting {
   return _isExecuting;
}

#pragma mark - NSURLConnectionDelegate Methods
#pragma mark Error Handling
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//   NSLog(@"(%s) connection (%@) error (%@)<%@>", __PRETTY_FUNCTION__, connection, error.localizedDescription, error.userInfo);
   if (error.code == NSURLErrorUserCancelledAuthentication) {
      [self performSelectorOnMainThread:@selector(authenticationDidFailWithError:) withObject:error waitUntilDone:NO];
   }

   self.error = error;

   [self makeDone];
}

#pragma mark Authentication Handling
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//   NSLog(@"(%s) connection (%@) challenge (%@)", __PRETTY_FUNCTION__, connection, challenge);
   /*
   AIRLog(@"didReceiveAuthenticationChallenge");

   if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
      BOOL ignoreSSL = [AIRSettings sharedSettings].ignoreSSLErrors;
      NSPredicate * ignoreSSLPredicate = [NSPredicate predicateWithValue:ignoreSSL];

      NSPredicate * hostPredicate = [NSPredicate predicateWithFormat:@"self.protectionSpace.host = %@",
                                     [self.secureAccount.eas_endpoint host]];

      NSPredicate * fullPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[hostPredicate, ignoreSSLPredicate]];

      if ([fullPredicate evaluateWithObject:challenge]) {
         AIRLog(@">>>>>>>>>>> Ignoring SSL Cert <<<<<<<<<<<");
         NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
         [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
      } else {
         [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
      }

   } else if ([[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodNTLM ||
              [[challenge protectionSpace] authenticationMethod] == NSURLAuthenticationMethodHTTPBasic) {
      self.lastProtectionSpace = [challenge protectionSpace];
//      / *
//       *  This is very, very important to check.  Depending on how your
//       *  security policies are setup, you could lock your user out of his or
//       *  her account by trying to use the wrong credentials too many times
//       *  in a row.
//       * /
      if ([challenge previousFailureCount] > 2) {
         [[challenge sender] cancelAuthenticationChallenge:challenge];
      } else {
         [[challenge sender]  useCredential:[self buildCredential] forAuthenticationChallenge:challenge];
      }

   } else {
      // we only support ntlm and basic
      [[challenge sender] cancelAuthenticationChallenge:challenge];
   }
    */
}

- (BOOL) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
//   NSLog(@"(%s) connection (%@) protectionSpace (%@)", __PRETTY_FUNCTION__, connection, protectionSpace);
   //   AIRLog(@"canAuthenticateAgainstProtectionSpace");
   //
   //   NSPredicate * predicate = [self allowedAuthenticationMethodsPredicate];
   //   BOOL result = [predicate evaluateWithObject:protectionSpace];
   //   return result;
   return NO;
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
//   NSLog(@"(%s) connection (%@)", __PRETTY_FUNCTION__, connection);
   /*
    // Validate credentials during login
    AIRLog(@"Validate credentials => %@", self.account.validateCredentials ? @"YES" : @"NO");
    return !self.secureAccount.validateCredentials;
    */
   return NO;
}

#pragma mark - NSURLConnectionDataDelegate Methods
#pragma mark Response Handling

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//   NSLog(@"(%s) connection (%@) response (%@)", __PRETTY_FUNCTION__, connection, response);

   self.response = (NSHTTPURLResponse *)response;

   if ([self.response statusCode] == 451) {
      // http://msdn.microsoft.com/en-us/library/gg651019(v=exchg.80).aspx
      // 451 is MS Specific redirect see MS-ASHTTP (above link)

      [self.urlConnection cancel];
      NSMutableString *oldURLString = [[self.request.URL absoluteString] mutableCopy];
      [oldURLString replaceOccurrencesOfString:[self.request.URL host]
                                    withString:[self.response allHeaderFields][@"X-MS-Location"]
                                       options:NSLiteralSearch
                                         range:NSMakeRange(0, [oldURLString length])];
      NSURL *newURL = [NSURL URLWithString:oldURLString];
      NSMutableURLRequest *newRequest = [self.request mutableCopy];
      newRequest.URL = newURL;
      self.request = newRequest;

      NSRunLoop *rl = [NSRunLoop currentRunLoop];
      self.urlConnection = [[NSURLConnection alloc] initWithRequest:self.request delegate:self startImmediately:NO];
      [self.urlConnection scheduleInRunLoop:rl forMode:NSRunLoopCommonModes];
      [self.urlConnection start];

   } else if ([self.response statusCode] != 200) {
      self.error = [NSError errorWithDomain:@"AIREASCommand" code:102 userInfo:@{@"statusCode" : @([self.response statusCode])}];

      [self cleanCredentialStore];

   }
}

#pragma mark Data Handling
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//   NSLog(@"(%s) connection (%@) data[%d:%d]", __PRETTY_FUNCTION__, connection, data.length, self.data.length);
   [self.data appendData:data];

   if (self.progressBlock) {
      self.progressBlock(self.data.length);
   }
}

#pragma mark Finish Handling
- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
//   NSLog(@"(%s) connction (%@)", __PRETTY_FUNCTION__, connection);
   if (self.error) {
      NSMutableDictionary *_dic = [[NSMutableDictionary alloc] initWithDictionary:self.error.userInfo];
      if (self.data) {
         NSString *result = [NSString stringWithUTF8String:(const char*)[self.data bytes]];
         _dic[@"HTMLResult"] = (result) ? result : @"";
      }
      self.error = [NSError errorWithDomain:self.error.domain
                                       code:self.error.code
                                   userInfo:_dic];
   }

   [self makeDone];
}

@end
