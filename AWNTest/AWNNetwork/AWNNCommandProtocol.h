//
//  AWNNCommandProtocol.h
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * @brief Enumeration of the different types of command requests. The full 
 * wealth of them can only be used in the \c NSURLSession implementation.
 * 
 * @note Currently, \c AWNNCommandOperation support Priority (using the 
 * \c AWNNCommandManager's \c priorityQueue) and  everything else (using the
 * concurrentQueue).
 */
typedef NS_ENUM(NSInteger, AWNNCommandType) {
   AWNNCommandTypeDefault = 0, /// Default command (concurrent data)
   AWNNCommandTypePriority,    /// Priority download command
   AWNNCommandTypeBackground,  /// Priority background download command
   AWNNCommandTypeDownload,    /// Concurrent download command
   AWNNCommandTypeData,        /// Concurrent data command
   AWNNCommandTypeUpload       /// Concurrent upload command
};

typedef void (^CompletionBlock)(NSError * error);
typedef void (^ProgressBlock)(NSUInteger progress);
typedef void (^RequestBlock)(NSHTTPURLResponse * response, NSData * responseData, NSError * responseError);
typedef void (^TaskCompletionBlock)(NSData *data, NSURLResponse *response, NSError *error);

@class SecureAccount;
@class AWNNCommandTask;

/**
 * @brief Default functionality for commands (whether \c AWNNCommandOperation 
 * or \c AWNNCommandTask objects).
 */
@protocol AWNNCommandProtocol <NSObject>

/// SecureAccount used for authorization challenges
@property (nonatomic, readonly, strong) SecureAccount * secureAccount;

/// Date of request
@property (nonatomic, readwrite, strong) NSDate * requestDate;
/// Request sent
@property (nonatomic, readonly, strong) NSURLRequest * request;
/// Response from server
@property (nonatomic, readonly, strong) NSHTTPURLResponse * response;
/// Possible data from the request
@property (nonatomic, readonly, strong) NSData * data;
/// Possible error from request
@property (nonatomic, readonly, strong) NSError * error;

/***
 * @brief send a standard \code{non multi-part} request
 * 
 * @param urlRequest to use
 * @param secureAccount for use with authorization request
 * @param progressBlock optional progress code block
 * @param requestBlock optional request code block
 */
- (void) sendRequest:(NSURLRequest *)urlRequest
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock;

/***
 * @brief send a possible multi-part request
 *
 * @param urlRequest to use
 * @param data to use in the \c multi-part request
 * @param secureAccount for use with authorization request
 * @param progressBlock optional progress code block
 * @param requestBlock optional request code block
 */
- (void) sendRequest:(NSURLRequest *)urlRequest
                Data:(NSData *)data
       SecureAccount:(SecureAccount *)secureAccount
       ProgressBlock:(ProgressBlock)progressBlock
        RequestBlock:(RequestBlock)requestBlock;

/***
 * @brief cancel a running command
 */

- (void)cancel;

@end

/**
 * @brief Manager functionality so that the \c AWNNCommandManager can better
 * manage authentication challenges and manage the list of commands that the 
 * manager manages.
 */
@protocol AWNNCommandManagerProtocol <NSObject>

/**
 * @brief Alert the manager that the task/operation has finished
 * @param command \c AWNNCommandProtocol command
 * @param requestDate \c NSDate of requestStart
 */
- (void)taskFinished:(id<AWNNCommandProtocol>)command Date:(NSDate *)requestDate;

/**
 * @brief Alert the manager that the credential store should be cleaned
 * @param command \c AWNNCommandProtocol command
 */
- (void)cleanCredentialsStore:(id<AWNNCommandProtocol>)command;

/**
 * @brief Alert the manager that authentication for a \c AWNNCommandProtocol 
 * command failed with an error.
 * @param error Authentication error
 * @param command \c AWNNCommandProtocol command
 */
- (void)authenticatorFailedWithError:(NSError *)error Command:(id<AWNNCommandProtocol>)command;

/**
 * @brief Alert the manager that the \c AWNNCommandProtocol command was cancelled.
 * 
 * @param command \c AWNNCommandProtocol command
 */
- (void)cancelledCommand:(id<AWNNCommandProtocol>)command;

@end
