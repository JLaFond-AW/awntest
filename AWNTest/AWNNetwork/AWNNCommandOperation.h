//
//  AWNNCommandOperation.h
//  AWNTest
//
//  Created by JP LaFond on 5/7/14.
//  Copyright (c) 2014 VMware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AWNNCommandProtocol.h"

@interface AWNNCommandOperation : NSOperation <AWNNCommandProtocol>

/// Delegate to allow the commandOperation to communicate back to the command manager
@property (nonatomic, readwrite, strong) id<AWNNCommandManagerProtocol> managerDelegate;

@end
